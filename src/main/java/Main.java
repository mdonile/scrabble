/*
 *  Developer: Mark Donile
 *  Date: July 9, 2016
 *  Version: 1.3.3
 */

import scrabble.Dictionary;
import scrabble.Node;
import scrabble.Rack;
import scrabble.ScrabbleAgent;

import util.DictionaryLoader;
import util.RackLoader;

import java.time.Instant;

import java.util.List;

public class Main {

    public static void main(String args[]){
        System.out.println("Starting scrabble game.\n");

        //load dictionary
        Dictionary dictionary = new DictionaryLoader().loadDictionary("SOWPODS.txt");

        //load racks from file
        List<Rack> rackList = new RackLoader().loadRack("test_rack_file.txt");

        //display racks
        System.out.println("The loaded racks are:");
        for(Rack rack : rackList){
            System.out.println(rack);
        }
        System.out.println();

        //sort racks in descending order of tile point value
        System.out.println("After sorting in descending order of tile point value:");
        for(Rack rack : rackList){
            rack.sortTilesByPoints();
            System.out.println(rack);
        }
        System.out.println();

        ScrabbleAgent scrabbleAgent = new ScrabbleAgent();
        scrabbleAgent.setDictionary(dictionary);

        //return results for each rack in test_rack_file.txt
        for(Rack rack : rackList){
            Node sNode = new Node();
            sNode.getCurrentPly().setRack(rack);
            sNode.setStartTime(Instant.now());

            Node eNode = scrabbleAgent.initialMove(sNode);
            eNode.setEndTime(Instant.now());

            System.out.println(eNode);
        }

    }

}
