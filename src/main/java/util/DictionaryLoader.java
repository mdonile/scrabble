/*
 *  Developer: Mark Donile
 *  Date: July 9, 2016
 */

package util;

import scrabble.Dictionary;

import java.io.File;
import java.io.FileReader;

import java.util.HashSet;
import java.util.Set;

public class DictionaryLoader {

    public Dictionary loadDictionary(String filename){

        Set<String> stringSet = new HashSet<>();

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        try(FileReader fileReader = new FileReader(file)){
            StringBuilder word = new StringBuilder();
            int i;
            while((i = fileReader.read()) != -1){
                char c = (char) i;
                if(c != '\r'){
                    if(c != '\n'){
                        word.append(c);
                    }
                    else{
                        if(word.length() > 1 && word.length() <= 7){
                            stringSet.add(word.toString());
                        }
                        word = new StringBuilder();
                    }
                }
            }
        }

        catch(Exception e){
            System.out.println(e.getMessage());
        }

        Dictionary dictionary = new Dictionary();
        dictionary.setDictionary(stringSet);

        return dictionary;
    }
}
