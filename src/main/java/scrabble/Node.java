/*
 *  Developer: Mark Donile
 *  Date: July 9, 2016
 */


package scrabble;

import java.time.Duration;
import java.time.Instant;

public class Node {


    //field(s)
    private int cost;
    private Ply currentPly;
    private int depth;
    private Instant endTime;
    private Ply maxScorePly;
    private Node parent;
    private Instant startTime;


    //constructor(s)
    public Node(){
        this.currentPly = new Ply();
        this.parent = null;
        this.maxScorePly = new Ply();
    }

    public Node(Node parent){
        this.currentPly = new Ply(parent.getCurrentPly());
        this.parent = parent;
        this.startTime = parent.getStartTime();
        this.maxScorePly = new Ply(parent.getMaxScorePly());
    }


    //public method(s)
    public String elapsedTime(){
        if(endTime == null){
            endTime = Instant.now();
        }
        Duration duration = Duration.between(startTime, endTime);
        long ms = duration.toMillis();
        return String.format("%d ms", ms);
    }

    public Ply getCurrentPly(){
        return currentPly;
    }

    public void setCurrentPly(Ply currentPly){
        this.currentPly = currentPly;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public Ply getMaxScorePly() {
        return maxScorePly;
    }

    public void setMaxScorePly(Ply maxScorePly) {
        this.maxScorePly = maxScorePly;
    }

    public Node getParent() {
        return parent;
    }

    public void setParent(Node parent) {
        this.parent = parent;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    @Override
    public String toString(){
        if(!maxScorePly.isExchange()){
            String rack = String.format("RACK: %48s", maxScorePly.getRack().toString());
            String word = String.format("PLACE WORD: " + getMaxScorePly().toString());
            String time = String.format("ELAPSED TIME: %8s", elapsedTime());
            return rack + " " + word + " " + time;
        }
        else{
            return maxScorePly.toString();
        }
    }

}
