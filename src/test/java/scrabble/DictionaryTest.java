package scrabble;


import org.testng.annotations.Test;
import util.DictionaryLoader;


public class DictionaryTest {

    @Test
    public void checkAllPermutationsOfBlankTilesTest(){
        Tile tile1 = new Tile('Q');
        Tile tile2 = new Tile('_');
        Tile tile3 = new Tile('I');
        Tile tile4 = new Tile('P');
        Tile tile5 = new Tile('_');

        Word word = new Word();
        word.append(tile1);
        word.append(tile2);
        word.append(tile3);
        word.append(tile4);
        word.append(tile5);

        Dictionary dictionary = new DictionaryLoader().loadDictionary("SOWPODS.txt");

        if(dictionary.contains(word)){
            System.out.println("Dictionary contains " + word.toString());
        }
        else{
            System.out.println("Dictionary failed to find " + word.toString());
        }

    }

}
