package scrabble;

import java.util.ArrayList;
import java.util.List;

public class Word {


    //field(s)
    private List<Tile> tileList;


    //constructor(S)
    public Word(){
        tileList = new ArrayList<>();
    }

    public Word(Word word){
        if(word != null){
            this.tileList = new ArrayList<>(word.getTileList());
        }
        else{
            this.tileList = new ArrayList<>();
        }
    }

    public Word(List<Tile> word){
        this.tileList = new ArrayList<>(word);
    }


    //public method(s)
    public List<Tile> getTileList(){
        return tileList;
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(Tile tile : tileList){
            sb.append(tile.getLetter());
        }
        return sb.toString();
    }

    public boolean contains(char c){
        boolean result = false;
        for(Tile tile : tileList){
            if(tile.getLetter() == c){
                result = true;
            }
        }
        return result;
    }

    public void append(Tile tile){
        tileList.add(tile);
    }

    public int size(){
        return tileList.size();
    }

}
