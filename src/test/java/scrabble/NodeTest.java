package scrabble;

import org.testng.annotations.Test;

import java.time.Instant;

public class NodeTest {

    @Test
    public void elapsedTimeTest(){
        Node node = new Node();
        node.setStartTime(Instant.now());
        node.setEndTime(Instant.now());
        System.out.println(node.elapsedTime());
    }
}
