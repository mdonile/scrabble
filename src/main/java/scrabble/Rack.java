package scrabble;

import java.util.ArrayList;
import java.util.List;

public class Rack{

    //field(s)
    List<Tile> tileList;


    //constructor(s)
    public Rack(){
        tileList = new ArrayList<Tile>();
    }

    public Rack(Rack rack){
        this.tileList = new ArrayList<>(rack.getTileList());
    }


    //public method(s)
    public void addTile(Tile tile){
        tileList.add(tile);
    }

    public List<Tile> getTileList(){
        return tileList;
    }

    public boolean isEmpty(){
        return tileList.isEmpty();
    }

    public Tile leastValuableTile(){
        Tile lvt = null;
        for(Tile tile : tileList){
            if(lvt != null){
                if(lvt.getPoints() > tile.getPoints()){
                    lvt = tile;
                }
            }
            else{
                lvt = tile;
            }
        }
        return lvt;
    }

    public void remove(Tile tile){
        tileList.remove(tile);
    }


    public void removeTile(int index){
        tileList.remove(index);
    }

    public void sortTilesByPoints(){
        tileList.sort((tile1, tile2) -> compare(tile1,tile2));
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < tileList.size(); i++){
            Tile tile = tileList.get(i);
            sb.append(tile.getLetter() + "(" + tile.getPoints() +")");
            if(i < tileList.size() - 1){
                sb.append(", ");
            }
        }
        return sb.toString();
    }


    //private method(s)
    private int compare(Tile tile1, Tile tile2){
        int tile1Points = tile1.getPoints();
        int tile2Points = tile2.getPoints();

        if(tile1Points > tile2Points){
            return -1;
        }
        else if(tile1Points < tile2Points){
            return 1;
        }
        else{ // tile1Points == tile2Points)
            return 0;
        }
    }

}
