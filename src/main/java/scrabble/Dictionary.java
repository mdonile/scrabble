package scrabble;


import java.util.Set;


public class Dictionary {


    //field(s)
    private Set<String> dictionary;


    //constructor(s)
    public Dictionary(){

    }


    //public method(s)
    public boolean contains(Word word){
        if(word.contains('_')){
            return checkAllPermutationsOfBlankTiles(word);
        }
        else{
            return dictionary.contains(word.toString());
        }
    }

    public boolean checkAllPermutationsOfBlankTiles(Word parentWord){
        Word word = new Word(parentWord);
        if(dictionary.contains(word.toString())){
            return true;
        }
        else if(word.contains('_')){
            for(Tile tile : word.getTileList()){
                if(tile.getLetter() == '_'){
                    for(char c = 'A'; c <= 'Z'; c++){
                        tile.setLetter(c);
                        boolean isFound = checkAllPermutationsOfBlankTiles(word);
                        tile.setLetter('_');
                        if(isFound){
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }


    public void setDictionary(Set<String> dictionary){
        this.dictionary = dictionary;
    }

}
