/*
 *  Developer: Mark Donile
 *  Date: July 9, 2016
 */

package scrabble;

public class Tile {

    //field(s)
    private char letter;
    private int points;


    //constructor(s)
    public Tile(char letter){
        this.letter = letter;
        this.points = setPoints();
    }


    //public method(s)
    public char getLetter() {
        return letter;
    }

    public void setLetter(char letter) {
        this.letter = letter;
    }

    public int getPoints() {
        return points;
    }

    @Override
    public String toString(){
        String s = String.format(letter + "(%d)", points);
        return s;
    }

    //private method(s)
    private int setPoints() {
        if(letter == 'A' || letter == 'E' || letter == 'I' || letter == 'O' || letter == 'L' || letter == 'N' ||
           letter == 'S' || letter == 'T' || letter == 'R' || letter == 'U'){
            return 1;
        }
        else if(letter == 'D' || letter == 'G'){
            return 2;
        }
        else if(letter == 'B' || letter == 'C' || letter == 'M' || letter == 'P'){
            return 3;
        }
        else if(letter == 'F' || letter == 'H' || letter == 'V' || letter == 'W' || letter == 'Y'){
            return 4;
        }
        else if(letter == 'K'){
            return 5;
        }
        else if(letter == 'J' || letter == 'X'){
            return 8;
        }
        else if(letter == 'Q' || letter == 'Z'){
            return 10;
        }
        else{ //blank tile, which is represented by the char '_'
            return 0;
        }
    }

}
