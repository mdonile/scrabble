/*
 *  Developer: Mark Donile
 *  Date: July 9, 2016
 */

package scrabble;


public class Ply {


    //CONSTANT(S)
    private static final int CENTER_COLUMN = 8;


    //field(s)
    private boolean exchange;
    private Tile exchangedTile;
    private int maxScore;
    private boolean scored;
    private int startColumn;
    private Word word;
    private Rack rack;


    //constructor(s)
    Ply(){
        this(0, 1, null);
    }

    Ply(Ply ply){
        this.maxScore = ply.getMaxScore();
        this.scored = false;
        this.startColumn = ply.getStartColumn();
        this.rack = new Rack(ply.getRack());
        this.word = new Word(ply.getWord());
    }

    Ply(int score, int startColumn){
        this(score, startColumn, null);
    }

    Ply(int score, int startColumn, Word word){
        this(score, startColumn, word, null);
    }

    Ply(int score, int startColumn, Word word, Rack rack){
        this.maxScore = score;
        this.scored = false;
        this.startColumn = startColumn;
        if(rack != null){
            this.rack = new Rack(rack);
        }
        else{
            this.rack = new Rack();
        }
        if(word != null){
            this.word = new Word(word);
        }
        else{
            this.word = new Word();
        }
    }

    //public method(s)
    public void exchange(){
        exchangedTile = rack.leastValuableTile();
    }

    public boolean isExchange(){
        return exchange;
    }

    public void isExchange(boolean exchange){
        this.exchange = exchange;
    }

    public int getMaxScore(){
        if(isScored()){
            return maxScore;
        }
        else{
            maxScore = calculateMaxScore();
            scored = true;
            return maxScore;
        }
    }

    public boolean isScored(){
        return scored;
    }

    public void setScored(boolean scored){
        this.scored = scored;
    }

    public int getStartColumn() {
        return startColumn;
    }

    public void setStartColumn(int startColumn) {
        this.startColumn = startColumn;
    }

    public Rack getRack() {
        return rack;
    }

    public void setRack(Rack rack) {
        this.rack = rack;
    }

    public Word getWord() {
        return word;
    }

    public void setWord(Word word) {
        this.word = word;
    }

    @Override
    public String toString(){
        if(!isExchange()){
            String sWord = String.format("%7s", word);
            String sStartColumn = String.format("COLUMN: %2d", startColumn);
            String sScore = String.format("SCORE: %3d", maxScore);
            return sWord + " " + sStartColumn + " " + sScore;
        }
        else{
            return "EXCHANGE: " + exchangedTile.toString();
        }
    }


    //private method(s)
    private int calculateMaxScore(){
        int maxScore = 0;
        int lmc = leftMostColumn(word);
        for(int col = lmc; col <= CENTER_COLUMN; col++){
            int score = calculateScore(word, col);
            if(maxScore < score){
                maxScore = score;
                startColumn = col;
            }
        }
        scored = true;
        return maxScore;
    }

    private int leftMostColumn(Word word){
        return CENTER_COLUMN - word.size() + 1;
    }

    private int calculateScore(Word word, int startCol){
        int score = 0;
        int col = startCol;
        for(Tile tile : word.getTileList()){
            if(col == 4 || col == 12){
                //double letter score
                score = score + (2 * tile.getPoints());
            }
            else{
                //single letter score
                score = score + (1 * tile.getPoints());
            }
            col++;
        }
        //center square is double word score
        score = (score * 2);
        if(isBingo(word)){
            score = score + 50;
        }
        return score;
    }

    private boolean isBingo(Word word){
        boolean bingo = false;
        if(word.size() == 7){
            bingo = true;
        }
        return bingo;
    }

}
