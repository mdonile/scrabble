package scrabble;

public class ScrabbleAgent {


    //field(s)
    private Dictionary dictionary;


    //constructor(s)
    public ScrabbleAgent(){

    }


    //method(s)
    public Node getMaxScorePly(Node parent){
        //System.out.println(parent);
        if(parent.getCurrentPly().getRack().isEmpty()){
            return parent;
        }
        else{
            for(Tile tile : parent.getCurrentPly().getRack().getTileList()) {
                Node child = new Node(parent);
                Ply currentPly = child.getCurrentPly();
                Rack rack = currentPly.getRack();
                Word word = currentPly.getWord();
                rack.remove(tile);
                word.append(tile);
                if(dictionary.contains(word)) {
                    int childScore = currentPly.getMaxScore();
                    int maxScore = child.getMaxScorePly().getMaxScore();
                    if (maxScore < childScore) {
                        child.setMaxScorePly(currentPly);
                    }
                }
                getMaxScorePly(child);
                parent.setMaxScorePly(child.getMaxScorePly());
            }
            return parent;
        }
    }

    public Node initialMove(Node node){
        Rack originalRack = node.getCurrentPly().getRack();
        Node initialMoveNode = getMaxScorePly(node);
        Ply initialPly = initialMoveNode.getMaxScorePly();
        if(initialPly.getWord().size() == 0){
            initialPly.setRack(originalRack);
            initialPly.isExchange(true);
            initialPly.exchange();
        }
        return initialMoveNode;
    }

    public void setDictionary(Dictionary dictionary){
        this.dictionary = dictionary;
    }


}
