package util;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import scrabble.Rack;
import scrabble.Tile;

public class RackTest {

    private Rack rack;

    @BeforeClass
    public void beforeClass(){
        Tile tile1 = new Tile('Z');
        Tile tile2 = new Tile('A');
        Tile tile3 = new Tile('B');
        Tile tile4 = new Tile('E');
        Tile tile5 = new Tile('Q');
        Tile tile6 = new Tile('F');

        rack = new Rack();
        rack.addTile(tile1);
        rack.addTile(tile2);
        rack.addTile(tile3);
        rack.addTile(tile4);
        rack.addTile(tile5);
        rack.addTile(tile6);
    }

    @Test
    public void sortTilesByPointsTest(){
        rack.sortTilesByPoints();
        System.out.print(rack);
    }

    @Test
    public void removeTileTest(){
        rack.removeTile(0);
        System.out.print(rack);
    }

}
