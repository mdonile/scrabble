package util;

import scrabble.Rack;
import scrabble.Tile;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class RackLoader {

    public List<Rack> loadRack(String filename){

        List<Rack> rackList = new ArrayList<>();

        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource(filename).getFile());

        try(FileReader fileReader = new FileReader(file)){

            Rack rack = new Rack();
            int i;
            while((i = fileReader.read()) != -1){
                char c = (char) i;
                if(c != '\r'){
                    if(c != '\n'){
                        rack.addTile(new Tile(c));
                    }
                    else{
                        rackList.add(rack);
                        rack = new Rack();
                    }
                }
            }
        }

        catch(Exception e){
            System.out.println(e.getMessage());
        }

        return rackList;
    }
}
